#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 3, 5); //Macro to make sure left side = right side
    }
}
